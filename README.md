# Photo Browser

A simple React app that gets an array of photo-objects from JSONPlaceholder API, displays their thumbnails in a grid view. Once the user clicks a thumbnail, the app will take the user to a page where the full sized image is shown with the title of the photo. User can then click a button to go back to the thumbnail page.

## Few technical details

-   the data is fetched with axios
-   page routes are created with React Router
-   thumbnail-grid, modal window, circular progression for loading, back-button and some other visual things are made using Material UI

## Install & Run

npm install

npm start
