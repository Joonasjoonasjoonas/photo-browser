import React from "react";
import { AppRouter } from "./Components/AppRouter";

const AppContainer = () => (
	<>
		<h1 style={{ padding: 10 }}>A Simple Photo Browser</h1>
		<AppRouter></AppRouter>
	</>
);

export default AppContainer;
