import React from "react";
import { Modal, Box, CircularProgress } from "@mui/material";

export const ModalWindow = ({ errorMsg, loadingData, loadingPhotos }) => {
	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 400,
		bgcolor: "background.paper",
		border: "2px solid #000",
		boxShadow: 24,
		p: 4
	};

	return (
		<>
			<Modal open={true}>
				<Box sx={modalStyle}>
					{errorMsg !== "" && <div>{errorMsg}</div>}
					{errorMsg === "" && (loadingData || loadingPhotos) && (
						<div>
							Loading...
							<br />
							<CircularProgress />
						</div>
					)}
				</Box>
			</Modal>
		</>
	);
};
