import React, { useContext } from "react";

import { AppContext } from "../AppContext";
import { useNavigate } from "react-router-dom";
import { Card, Button } from "@mui/material";
import { ModalWindow } from "../ModalWindow";
import { useState } from "react";

export const FullImageView = () => {
	const { selectedPhoto } = useContext(AppContext);
	const [photoLoaded, setPhotoLoaded] = useState(false);
	const navigate = useNavigate();
	const contentStyle = {
		width: 600,
		maxWidth: 600,

		padding: 1
	};

	return (
		<>
			<Card sx={contentStyle} variant="outlined">
				<h3>{selectedPhoto.title}</h3>
				<img
					src={selectedPhoto.url}
					key={selectedPhoto.id}
					onLoad={() => setPhotoLoaded(true)}
				/>
				{console.log(selectedPhoto.url)}
				<Button variant="contained" onClick={() => navigate("/")}>
					Back
				</Button>
			</Card>
			{!photoLoaded && (
				<ModalWindow
					loadingData={false}
					loadingPhotos={true}
					errorMsg={""}
				/>
			)}
		</>
	);
};
