import React, { useContext, useState } from "react";

import { AppContext } from "../AppContext";
import { useNavigate } from "react-router-dom";
import { ModalWindow } from "../ModalWindow";
import { Grid } from "@mui/material";

const Thumbnails = () => {
	const { choosePhoto, photoData, loadingData, errorMsg } =
		useContext(AppContext);
	const [loadedPhotosAmount, setLoadedPhotosAmount] = useState(0);
	const [loadingPhotos, setLoadingPhotos] = useState(true);
	const navigate = useNavigate();

	const contentStyle = {
		width: 800,
		maxWidth: 800,
		display: "inline-block",
		padding: 10
	};

	const openLargePhoto = (photo) => {
		choosePhoto(photo);
		navigate("/fullimageview");
	};

	const finishedLoadingPhoto = () => {
		setLoadedPhotosAmount(loadedPhotosAmount + 1);

		if (loadedPhotosAmount === photoData.data.length - 1)
			setLoadingPhotos(false);
	};

	return (
		<>
			<h2 style={{ padding: 10 }}>Thumbnails:</h2>
			<div style={contentStyle}>
				<Grid container spacing={1}>
					{loadingData === false &&
						photoData !== null &&
						photoData.data.map((photo) => (
							<Grid item md={2.4} key={photo.id}>
								<img
									onLoad={() => finishedLoadingPhoto()}
									src={photo.thumbnailUrl}
									key={photo.id}
									onClick={() => openLargePhoto(photo)}
								/>
							</Grid>
						))}
				</Grid>
			</div>

			{(loadingData || errorMsg !== "" || loadingPhotos) && (
				<ModalWindow
					loadingData={loadingData}
					loadingPhotos={loadingPhotos}
					errorMsg={errorMsg}
				/>
			)}
		</>
	);
};

export default Thumbnails;
