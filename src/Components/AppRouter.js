import React, { useContext } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Thumbnails from "./Pages/Thumbnails";
import { FullImageView } from "./Pages/FullImageView";
import { AppContext } from "./AppContext";

export const AppRouter = () => {
	const { selectedPhoto } = useContext(AppContext);

	return (
		<div>
			<Router>
				<Routes>
					<Route path="/" element={<Thumbnails />} />
					{selectedPhoto !== {} && (
						<Route
							path="/fullimageview"
							element={
								<FullImageView selectedPhoto={selectedPhoto} />
							}
						/>
					)}
				</Routes>
			</Router>
		</div>
	);
};
