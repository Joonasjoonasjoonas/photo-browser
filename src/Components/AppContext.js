import React from "react";
import { createContext, useState, useEffect } from "react";
import axios from "axios";

export const AppContext = createContext();

export const AppProvider = ({ children }) => {
	const [selectedPhoto, setSelectedPhoto] = useState(null);
	const [photoData, setPhotoData] = useState(null);
	const [errorMsg, setErrorMsg] = useState("");
	const [loadingData, setLoadingData] = useState();

	const choosePhoto = (photo) => {
		setSelectedPhoto(photo);
	};

	useEffect(() => {
		const getPhotosFromAPI = () => {
			setLoadingData(true);
			axios({
				method: "get",
				url: "https://jsonplaceholder.typicode.com/photos?albumId=1"
			})
				.then((res) => {
					setPhotoData(res);
				})
				.catch((err) => {
					if (err.request)
						setErrorMsg("Error: Couldn't load photos!");
				})
				.finally(setLoadingData(false));
		};
		if (photoData === null) getPhotosFromAPI();
	}, []);

	return (
		<AppContext.Provider
			value={{
				photoData,
				selectedPhoto,
				choosePhoto,
				errorMsg,
				loadingData
			}}
		>
			{children}
		</AppContext.Provider>
	);
};
